public class Student{
	public double rScore;
	public int numOfClasses;
	public String eyeColour;	
	
	public void fullTime(){
		if(this.numOfClasses > 3){
			System.out.println("You are a full time student!");
		} else{
			System.out.println("You are not a full time student");
		}
	}
	
	public void blink(){
		System.out.println("You elegantly close your eyes for an instant to then reaveal once again your beautiful " + this.eyeColour + " eyes");
	}
}