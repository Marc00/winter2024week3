public class Application{
	public static void main(String args[]){	
		Student[] section3 = new Student[3];
		
		Student marco = new Student();
		Student maeva = new Student();
		
		section3[0] = marco;
		section3[1] = maeva;
		section3[2] = new Student();
		
		section3[2].rScore = 30.119;
		section3[2].numOfClasses = 5;
		section3[2].eyeColour = "brown";
		
		marco.rScore = 31.779;
		marco.numOfClasses = 5;
		marco.eyeColour = "green";
		
		maeva.rScore = 35.000;
		maeva.numOfClasses = 7;
		maeva.eyeColour = "brown";
		
		System.out.println(section3[2].rScore);
		System.out.println(section3[2].numOfClasses);
		System.out.println(section3[2].eyeColour);		
		
		//marco.blink();
		//maeva.blink();
	}
}